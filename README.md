# README #

JavaScript ES2019 component driven, dynamic import micro-framework.

This is not a library nor package, rather an example of concept implementation in real working scenario.

### What does this repository solve? ###

* Resolves front-end structures to satisfy [module design pattern](https://en.wikipedia.org/wiki/Module_pattern).

### What is this repository for? ###

* Super light start up foundation for building small to large scale modular front-end applications.
* JavaScript developers tutorial usage.

### Features ###

* No external libraries or frameworks used, raw vanilla JavaScript in action.
* Lazy load or load on demand of a component.
* Streamlines or centralizes the way the front-end components are initiated.
* Solid design pattern behind bonding html and js parts of a component.
* Comes with predefined components structure as an example.

### Focus ###

* Performance, transparency, maintainability, testability and consistency. :)
* Get the most out of the bare minimum.

### Dependencies ###

* None

### Application Class Structure ###
```
application
│
└───App
│   │   Nav
│   │   Footer
```

### Related Documentation ###
* [ES10 - Dynamic Import](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)

### Installation ###
```
        <script src="assets/js/App.js" type="text/javascript"></script>
    </body>
</html>
```

### Define Application Options ###
```
<html lang="en-US" data-options='{"optionAppOne": "Application option one value", "optionTwo": "Application option two value", "optionThree": "Application option three value"}'>
    <head>
        <title>...
```

### Define Components - HTML ###
Component options override application options.
```
<nav data-component="Nav" data-options='{"optionOne": "nav html option one value", "optionTwo": "nav html option two value"}'>
    ...
</nav>
```

```
<footer data-component="Footer" data-options='{"optionOne": "footer html option one value", "optionTwo": "footer html option two value"}'>
    ...
</footer>
```

### Define Components - JS ###
```
export class Nav extends App {
    constructor() {
      super();
      super.exampleMethod();
      console.log(this);
    }
}
```

```
export class Footer extends App {
    constructor() {
      super();
      super.exampleMethod();
      console.log(this);
    }
}
```

### Console Log ###
```
Nav.js:20 Nav {optionAppOne: 'Application option one value', optionTwo: 'Application option two value', optionThree: 'Application option three value'}optionAppOne: "Application option one value"optionOne: "nav html option one value"optionThree: "Application option three value"optionTwo: "nav html option two value"[[Prototype]]: App
```

```
Footer.js:20 Footer {optionAppOne: 'Application option one value', optionTwo: 'Application option two value', optionThree: 'Application option three value'}optionAppOne: "Application option one value"optionOne: "footer html option one value"optionThree: "Application option three value"optionTwo: "footer html option two value"[[Prototype]]: App
```