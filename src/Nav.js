/**
 * Description.         Provides class structure for Nav component.
 * 
 * @since 1.1.0
 * 
 */
export class Nav extends App {

    /**
     * Description.                     Initiate Nav component. 
     *                                  Initiate application and component html parsed options.
     * 
     * @class
     * @access  public
     * @returns {void}
     */
    constructor() {
      super();
      super.exampleMethod();
      console.log(this);
    }
}