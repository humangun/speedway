/**
 * Description.               Microfrawork that autoloads js components on demand. Bonds component's html with its js interface. 
 *         
 * @since      1.1.0
 * @static     {string}       optionsSelector           Html tag that is used to store and obtain options from.
 * @static     {string}       componentsSelector        Html tag attribute that is used to identify a component.
 * @static     {string}       componentPath             Relative root path where components reside.
 */
class App {
  static optionsSelector = 'html';
  static componentSelector = '[data-component]'; // or '[data-component="Nav"], [data-component="Footer"]'
  static componentPath = './';

  /**
   * Description.                     Defines options gathering logic. Extracts application wide options defined within html.
   * 
   * @class
   * @access  public
   * @returns {void}
   */
  constructor() {
    Object.assign(this, JSON.parse(document.querySelector(App.optionsSelector).dataset.options));
  }

  /**
   * Description.                     Scans and picks components from html. Imports and initiates comonent's class.
   * 
   * @access  public
   * @returns {void}                  Disable method chaining.
   */
  static run() {
    document.querySelectorAll(App.componentSelector).forEach(function (element) {
      let name = element.dataset.component;
      name = name.charAt(0).toUpperCase() + name.slice(1);

      (async () => {
        await import(App.componentPath + name + '.js').then(function(component) {
            component = new component[name]();
            Object.assign(component, JSON.parse(element.dataset.options));
        }).catch(error => {
            console.error(error);
        });
      })();
    });
  }

  /**
   * Description.                     Example of an application wide public method.
   * Usage.                           Within a component: super.exampleMethod(); or self.exampleMethod();
   * 
   * @access  protected               Cannot be accessed from outsite a component.
   * @returns {void}                
   */
  exampleMethod() {
    console.log('Application wide method');
  }
}

/**
 * Initiate the framework.
 */
App.run();