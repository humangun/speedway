/**
 * Description.         Provides class structure for Footer component.
 * 
 * @since 1.1.0
 * 
 */
export class Footer extends App {

    /**
     * Description.                     Initiate Footer component. 
     *                                  Initiate application and component html parsed options.
     * 
     * @class
     * @access  public
     * @returns {void}
     */
    constructor() {
      super();
      super.exampleMethod();
      console.log(this);
    }
}